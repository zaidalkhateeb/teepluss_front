import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from "@/store/store";
import axios from "axios";
import './shared/axios-config';


const UNAUTHORIZED = 401;
Vue.http = Vue.prototype.$http = axios;


axios.interceptors.request.use(function (config) {
  console.log(config)
  config.headers.Accept = '*';
  return config;
});

axios.interceptors.response.use(
    response => response,
    error => {
      console.log(error)
      const status = error?.response;
      if (status === UNAUTHORIZED) {
        store.dispatch('logout');
        router.push('/login')
      }
      return Promise.reject(error);
    }
);


new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
