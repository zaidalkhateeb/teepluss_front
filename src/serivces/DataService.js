
export  class  DataService {
    endPoint = '';

    getFullUrl(targetUrl) {
        return `${this.endPoint}/${targetUrl}`;
    }

    get(url){
        // eslint-disable-next-line no-undef
        return axios.get(this.getFullUrl(url));
    }

    post(url,data){
        // eslint-disable-next-line no-undef
        return axios.post(this.getFullUrl(url), data);
    }

    put(url,data){
        // eslint-disable-next-line no-undef
        return axios.put(this.getFullUrl(url), data);
    }

    delete(url){
        // eslint-disable-next-line no-undef
        return axios.delete(this.getFullUrl(url));
    }
}
