const mutations={
     logout:(state)=>{
         localStorage.removeItem('accessToken')
         localStorage.removeItem('user')
         state.accessToken = ''
         state.user={};
         // eslint-disable-next-line no-undef
         delete axios.defaults.headers.common['Authorization']
     },
    login:(state,data)=>{
        state.accessToken=data.access_token;
        state.user=data.user;
        localStorage.setItem('accessToken',state.accessToken)
        localStorage.setItem('user',JSON.stringify(state.user))
        // eslint-disable-next-line no-undef
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.accessToken;
    }
}
export default mutations;
