import Vue from 'vue';
import Vuex from 'vuex';

import state from './state';
import getters from "./getters";
import actions from "@/store/actions";
import mutations from "@/store/mutations";

Vue.use(Vuex);
export default new Vuex.Store({
   strict:true,
   state,
   getters,
   actions,
   mutations
});
