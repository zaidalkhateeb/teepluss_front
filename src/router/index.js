import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store/store";

Vue.use(VueRouter)

const routes = [
    {
        path: '/home',
        name: 'Home',
        // for lazy load
        component: () => import(/* webpackChunkName: "about" */ '../views/HomePage'),
        meta: {
            // requiresAuth: true,
            title: 'Home'
        }
    },
    {
        path: '/',
        redirect: '/home',

    },
    {
        path: '/edit_art_work',
        name: 'EditArtWork',
        component: () => import('../views/ArtWork/EditArtWork'),
        meta: {
            requiresAuth: true,
            canActive: true,
            title: 'Home'
        }
    },
    {
        path: '/signUp',
        name: 'Signup',
        component: () => import('../views/Auth/SignUp.vue'),
        meta: {
            title: 'signup'
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Auth/LogIn.vue'),
        meta: {
            title: 'Login'
        }
    },
    {
        path: '**',
        component: () => import('@/views/Auth/404Page'),
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
const DEFAULT_TITLE = 'Tee Plus Project';

router.beforeEach((to, from, next) => {
    Vue.nextTick(() => {
        document.title = to.meta.title || DEFAULT_TITLE;
    })
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.getters.IsLoggedIn) {
            next()
        } else {
            next('login')
        }
    } else {
        if (store.getters.IsLoggedIn && (to.fullPath === '/login' || to.fullPath === '/signup')) {
            next('home')
        } else {
            next() // make sure to always call next()!
        }

    }
})

export default router
