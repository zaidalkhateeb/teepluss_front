const getters = {
    IsLoggedIn: state => !!state.accessToken,
    userType: state=> state.user.type===2?"Artis":"User"
}
export default getters;
