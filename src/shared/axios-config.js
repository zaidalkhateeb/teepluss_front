import axios from 'axios' ;

window.axios = axios;

window.axios.defaults.headers.common['Accept'] = 'application/json';
window.axios.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('accessToken')
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Content-Type'] = '*';
window.axios.defaults.baseURL = process.env.VUE_APP_API_URL  + '/api';

