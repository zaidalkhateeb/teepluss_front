import store from "@/store/store";


const actions = {
    logout: (context) => {
            context.commit('logout')
    },
    login: (context, data) => {
        return store.state.authService.post('login', data).then((res) => {
            context.commit('login', res.data)
        })
    },
    signup: (context, data) => {
        return store.state.authService.post('signup', data).then((res) => {
            context.commit('login', res.data.data)
        })
    }
}
export default actions;
