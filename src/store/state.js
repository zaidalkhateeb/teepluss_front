import AuthService from "@/views/Auth/AuthService";
import ArtWorkService from "@/views/ArtWork/ArtWorkService";
import {AdminService} from "@/views/Admin/adminService";

const state = {
    accessToken: localStorage.getItem('accessToken') || '',
    user: JSON.parse(localStorage.getItem('user')) || {
        permissions: []
    },
    authService: new AuthService(),
    artWorkService: new ArtWorkService(),
    adminService: new AdminService(),
    hostUrl: process.env.VUE_APP_API_URL
}
export default state;
